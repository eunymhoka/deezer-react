import React from "react";
import Artistscard from "../components/Artistscard";
import NavBar from "../components/NavBar";
import SearchBar from "../components/SearchBar";
const Home =({artistsArray})=>{
    return(
        <>
        <NavBar />
        <SearchBar />
        <Artistscard />
        </>
    )
}
export default Home;