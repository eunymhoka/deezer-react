import React, {useState, useEffect} from "react";
import Home from "./pages/Home";
import './App.css';

const App=()=> {
  const [artists, setArtists] = useState([]);
  useEffect(()=>{
    fetch()
    .then((response) => response.json())
    .then((data)=>{
      setArtists(data)
    })
    .catch((err)=>{
      console.log(err);
    })
  })
  return (
    <div className='font-raleway'>
      <Home artistsArray = {artists}/>
    </div>
  );
}

export default App;
