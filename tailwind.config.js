/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],

  theme: {
    // colors: {
    //   'black': '#0B0D26',
    //   'orange': '#F2441D',
    //   'blue': '#1D79F2',
    //   'gray-dark': '#434459',
    //   'gray-light': '#F5F5F5',
    //   'gray': '#CCCCCC',
    //   'red': '#F2441D',
    //   "white": "#ffffff"
    // },
    fontFamily: {
      raleway: ['Raleway'],
      comfortaa: ['Comfortaa']
    },
    extend: {},
  },
  plugins: [],
}

